-- phpMyAdmin SQL Dump
-- version 4.7.9
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Creato il: Feb 07, 2021 alle 23:01
-- Versione del server: 10.1.31-MariaDB
-- Versione PHP: 7.2.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;

--
-- Database: booksandauthorsdemo
--
CREATE DATABASE IF NOT EXISTS booksandauthorsdemo DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE booksandauthorsdemo;

-- --------------------------------------------------------

--
-- Struttura della tabella authors
--
-- Creazione: Feb 03, 2021 alle 22:55
--

CREATE TABLE IF NOT EXISTS `authors` (
  author_id int(10) NOT NULL AUTO_INCREMENT,
  first_name varchar(100) NOT NULL,
  last_name varchar(100) NOT NULL,
  birth_year int(4) NOT NULL,
  PRIMARY KEY (author_id),
  KEY IX_BIRTH_YEAR (birth_year)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

--
-- Dump dei dati per la tabella authors
--

INSERT INTO authors (author_id, first_name, last_name, birth_year) VALUES
(1, 'Andrea', 'Camilleri', 1925),
(2, 'Edgar Allan', 'Poe', 1809),
(3, 'Agatha', 'Christie', 1890),
(4, 'Ken', 'Follett', 1949),
(5, 'Joanne Kathleen', 'Rowling', 1965),
(6, 'Stephen', 'King', 1974),
(7, 'Henry', 'James', 1843);

-- --------------------------------------------------------

--
-- Struttura della tabella books
--
-- Creazione: Feb 07, 2021 alle 22:57
--

CREATE TABLE IF NOT EXISTS books (
  isbn int(13) NOT NULL,
  title varchar(100) DEFAULT NULL,
  author_id int(10) NOT NULL,
  genre varchar(50) DEFAULT NULL,
  pages int(4) NOT NULL DEFAULT '-1',
  price int(10) NOT NULL,
  PRIMARY KEY (isbn),
  KEY IX_TITLE (title),
  KEY IX_GENRE (genre),
  KEY IX_PRICE (price),
  KEY IX_AUTHORID (author_id) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dump dei dati per la tabella books
--

INSERT INTO books (isbn, title, author_id, genre, pages, price) VALUES
(1000000000, 'pilastri della terra', 4, 'fiction storica', 900, 45),
(1111111111, 'Km 123', 1, 'Noire', 180, 10),
(1111115559, 'It', 6, 'Horror', 250, 25),
(1116668883, 'Animali fantastici', 5, 'Fantasy', 400, 26),
(1147025803, 'Riccardino', 1, 'Romanzo', 260, 15),
(1225879546, 'Harry Potter ed il principe mezzosangue', 5, 'Fantasy', 450, 20),
(1234566548, 'Shining', 6, 'Horror', 1000, 50),
(1234567890, 'Come la penso', 1, 'Biografia', 250, 30),
(1234567897, 'Il foglio', 7, 'Horror', 199, 9),
(1444444444, 'Avverture di Gordon Pym', 2, 'Romanzo', 365, 40),
(1451231698, 'Harry Potter ed il calice di fuoco', 5, 'Fantasy', 400, 14),
(1457895632, 'Le fiabe di Beda il bardo', 5, 'Fantasy', 150, 10),
(1555555555, 'Il gatto nero', 2, 'Thriller', 300, 21),
(1591591592, 'L\'americano', 7, 'Romanzo', 26, 22),
(1591741326, 'Harry Potter e la camera dei segreti', 5, 'Fantasy', 250, 15),
(1598742360, 'Harry Potter ed i doni della morte', 5, 'Fantasy', 700, 25),
(1666666666, 'Le due verità', 3, 'giallo', 250, 8),
(1777777777, 'Ultima seduta spiritica', 3, 'Giallo', 220, 5),
(1777888333, 'Dr. Sleep', 6, 'Horror', 300, 28),
(1879453698, 'Harry Potter e l\'ordine della fenice', 5, 'Fantasy', 450, 20),
(1888888888, 'Assassinio sull\'OrientExpress', 3, 'Giallo', 400, 26),
(1981551630, 'Harry Potter ed il prigioniero di Azkaban', 5, 'Fantasy', 300, 17),
(1987654321, 'Ora dimmi di te: lettere a Matilda', 1, 'Biografico', 190, 22),
(1999999999, 'Fu sera e fu mattina', 4, 'fiction storica', 700, 40),
(2000000000, 'Harry Potter e la pietra filosofale', 5, 'Fantasy', 200, 12),
(2122222222, 'Racconti del mistero', 2, 'Horror', 400, 36);

--
-- Limiti per le tabelle scaricate
--

--
-- Limiti per la tabella books
--
ALTER TABLE books
  ADD CONSTRAINT BOOKS_AUTHOR FOREIGN KEY (author_id) REFERENCES authors (author_id) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;