package com.example.BooksAndAuthorsDemo.model.Entities;


import javax.persistence.*;

@Entity
@Table(name="books")
public class Book {

//tracciamento della versione della classe
    private static final long serialVersionUID = 1L;

//mapping delle colonne della tabella
    @Id
    @Column(name = "isbn")
    private int isbn;

    @Column(name = "title")
    private String title;

    @Column(name = "genre")
    private String genre;

    @Column(name = "pages")
    private int pages;

    @Column(name = "price")
    private float price;

//gestione della relazione tra libro e autore
    @ManyToOne()
    @JoinColumn(name = "author_id", referencedColumnName = "author_id")
    private Author author;


// getter e setter
    public int getIsbn() {
        return isbn;
    }

    public void setIsbn(int isbn) {
        this.isbn = isbn;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getGenre() {
        return genre;
    }

    public void setGenre(String genre) {
        this.genre = genre;
    }

    public int getPages() {
        return pages;
    }

    public void setPages(int pages) {
        this.pages = pages;
    }

    public float getPrice() {
        return price;
    }

    public void setPrice(float price) {
        this.price = price;
    }

    public Author getAuthor() {
        return author;
    }

    public void setAuthor(Author author) {
        this.author = author;
    }
}
