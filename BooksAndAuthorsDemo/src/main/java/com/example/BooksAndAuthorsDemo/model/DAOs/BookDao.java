package com.example.BooksAndAuthorsDemo.model.DAOs;

import com.example.BooksAndAuthorsDemo.model.Entities.Author;
import com.example.BooksAndAuthorsDemo.model.Entities.Book;
import com.example.BooksAndAuthorsDemo.model.repos.BookRepository;
import com.example.BooksAndAuthorsDemo.utils.StaticUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class BookDao
{
    //utilizzo i metodi del CrudRepository senza istanziare nulla tramite Autowired
    @Autowired
    private BookRepository br;

    //metodo per ricavare tutti i libri del catalogo
    public List<Book> findAll()
    {
        List <Book> result = new ArrayList<>();
        Iterable <Book> tmp = br.findAll();
        for (Book book : tmp)
        {
            result.add(book);
        }
        return result;
    }

    //motodo per trovare il singolo libro
    public Book findById(int id)
    {
        Book result = null;

        if (br.existsById(id))
        {
            result = br.findById(id).get();
        }

        return result;
    }

    //dato che il metodo save() si usa sia per inserire nuovi record che per modificare uno già esistente, accorpo le due operazioni
    public Book createOrUpdateBook(Book book)
    {
        boolean isOk = StaticUtils.checkBookIntegrity(book);
        Book result = null;
        if (isOk)
        {
            result = br.save(book);
        }

        return result;
    }

    //metodo di eliminazione di un libro dato l'ISBN
    public boolean deleteBook(Book book)
    {
        boolean deleted = false;
        if (br.existsById(book.getIsbn()))
        {
            br.delete(book);
            deleted = true;
        }
        return deleted;
    }

    public List<Book> findAllByAuthor (Author a)
    {
        return br.findAllByAuthor(a);
    }

}