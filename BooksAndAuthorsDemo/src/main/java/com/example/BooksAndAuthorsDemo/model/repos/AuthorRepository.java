package com.example.BooksAndAuthorsDemo.model.repos;

import com.example.BooksAndAuthorsDemo.model.Entities.Author;
import org.springframework.data.repository.CrudRepository;

public interface AuthorRepository extends CrudRepository<Author, Integer> {
}
