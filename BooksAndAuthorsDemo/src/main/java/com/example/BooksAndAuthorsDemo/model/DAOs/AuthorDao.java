package com.example.BooksAndAuthorsDemo.model.DAOs;

import com.example.BooksAndAuthorsDemo.model.Entities.Author;
import com.example.BooksAndAuthorsDemo.model.Entities.Book;
import com.example.BooksAndAuthorsDemo.model.repos.AuthorRepository;
import com.example.BooksAndAuthorsDemo.model.repos.BookRepository;
import com.example.BooksAndAuthorsDemo.utils.StaticUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class AuthorDao
{
    //utilizzo i metodi del CrudRepository senza istanziare nulla tramite Autowired
    @Autowired
    private AuthorRepository ar;

    @Autowired
    private BookRepository br;

    //metodo per ricavare tutti gli autori
    public List<Author> findAll()
    {
        List <Author> result = new ArrayList<>();
        Iterable <Author> tmp = ar.findAll();
        for (Author author : tmp)
        {
            result.add(author);
        }
        return result;
    }

    //motodo per trovare il singolo autore
    public Author findById(int id)
    {
        Author result = null;

        if (ar.existsById(id))
        {
            result = ar.findById(id).get();
        }

        return result;
    }

    //dato che il metodo save() si usa sia per inserire nuovi record che per modificare uno già esistente, accorpo le due operazioni
    public Author createOrUpdateAuthor(Author author)
    {
        boolean isOk = StaticUtils.checkAuthorIntegrity(author);
        Author result = null;
        if (isOk)
        {
            result = ar.save(author);
        }

        return result;
    }

    //metodo di eliminazione di un autore dato l'ID
    public boolean deleteAuthor(Author author)
    {
        boolean deleted = false;
        if (ar.existsById(author.getId()))
        {
            List<Book> authorBooks = br.findAllByAuthor(author);
            for (Book b : authorBooks)
            {
                br.delete(b);
            }
            ar.delete(author);
            deleted = true;
        }
        return deleted;
    }




}
