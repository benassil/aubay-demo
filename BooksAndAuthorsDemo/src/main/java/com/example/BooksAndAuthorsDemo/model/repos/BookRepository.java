package com.example.BooksAndAuthorsDemo.model.repos;

import com.example.BooksAndAuthorsDemo.model.Entities.Author;
import com.example.BooksAndAuthorsDemo.model.Entities.Book;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public interface BookRepository extends CrudRepository<Book, Integer>
{
    @Query("SELECT b FROM Book b WHERE b.author = :author")
    public List<Book> findAllByAuthor(@Param("author") Author author);
}
