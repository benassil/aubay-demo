package com.example.BooksAndAuthorsDemo.utils;

import com.example.BooksAndAuthorsDemo.model.Entities.Author;
import com.example.BooksAndAuthorsDemo.model.Entities.Book;

public class StaticUtils
{
    //controllo che tutti i campi del libro siano correttamente compilati
    public static boolean checkBookIntegrity(Book book)
    {
        boolean isOk = true;

        //il codice ISBN può avere solo 10 o 13 cifre
        if (String.valueOf(book.getIsbn()).length() != 13 && String.valueOf(book.getIsbn()).length() != 10)
        {
            isOk = false;
        }
        else if (book.getGenre() == null || book.getGenre().length() == 0)
        {
            isOk = false;
        }
        else if (!checkAuthorIntegrity(book.getAuthor()))
        {
            isOk = false;
        }
        else if (book.getPages() == 0 || String.valueOf(book.getPages()).length() == 0)
        {
            isOk = false;
        }
        else if (book.getTitle() == null || book.getTitle().length() == 0)
        {
            isOk = false;
        }
        else if (String.valueOf(book.getPrice()).length() == 0)
        {
            isOk = false;
        }

        return isOk;
    }

    //controllo che tutti i campi dell'autore siano correttamente compilati
    public static boolean checkAuthorIntegrity(Author author)
    {
        boolean isOk = true;

        if (String.valueOf(author.getBirthYear()).length() == 0)
        {
            isOk = false;
        }
        else if (author.getFirstName().length() == 0 || author.getFirstName() == null)
        {
            isOk = false;
        }
        else if (author.getLastName().length() == 0 || author.getLastName() == null)
        {
            isOk = false;
        }
        else if (String.valueOf(author.getId()).length() == 0 || author.getId() < 0)
        {
            isOk = false;
        }

        return isOk;
    }
}
