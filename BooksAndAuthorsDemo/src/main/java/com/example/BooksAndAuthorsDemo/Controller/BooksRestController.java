package com.example.BooksAndAuthorsDemo.Controller;

import com.example.BooksAndAuthorsDemo.model.DAOs.BookDao;
import com.example.BooksAndAuthorsDemo.model.Entities.Book;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/books")
public class BooksRestController
{
    @Autowired
    private BookDao bd;

    @CrossOrigin(origins = "*")
    @GetMapping(value = "/getAll")
    public ResponseEntity<Object> findAllBooks()
    {
        try {
            List<Book> outcome = bd.findAll();;

            if ( outcome == null)
            {
                return ResponseEntity.notFound().build();
            }
            return ResponseEntity.ok(outcome);
        }
        catch (Exception e) {
            e.printStackTrace();
            return ResponseEntity.badRequest().body("Cannot find any book due to the following exception: " + e.getCause());
        }
    }

    @CrossOrigin(origins = "*")
    @GetMapping(value = "/get/{id}")
    public ResponseEntity<Object> findSingleBook(@PathVariable int id)
    {
        try {
            Book outcome = bd.findById(id);;

            if ( outcome == null)
            {
                return ResponseEntity.notFound().build();
            }
            return ResponseEntity.ok(outcome);
        }
        catch (Exception e) {
            e.printStackTrace();
            return ResponseEntity.badRequest().body("Cannot find book due to the following exception: " + e.getCause());
        }
    }

    @CrossOrigin(origins = "*")
    @PostMapping(value = "/newBook")
    public ResponseEntity<Object> addNewBook(@RequestBody Book newBook)
    {
        try {
            Book outcome = bd.createOrUpdateBook(newBook);

            if ( outcome == null)
            {
                return ResponseEntity.badRequest().body("Book information is not correct or incomplete");
            }
            return ResponseEntity.ok(newBook);
        }
        catch (Exception e)
        {
            e.printStackTrace();
            return ResponseEntity.badRequest().body("Cannot create new book due to the following exception: " + e.getCause());
        }
    }

    @CrossOrigin(origins = "*")
    @PostMapping(value = "/updateBook")
    public ResponseEntity<Object> updateBook(@RequestBody Book updatedBook)
    {
        try {
            Book outcome = bd.createOrUpdateBook(updatedBook);

            if ( outcome == null)
            {
                return ResponseEntity.badRequest().body("Book information is not correct or incomplete");
            }
            return ResponseEntity.ok(outcome);
        }
        catch (Exception e) {
            e.printStackTrace();
            return ResponseEntity.badRequest().body("Cannot update the book due to the following exception: " + e.getCause());
        }
    }

    @CrossOrigin(origins = "*")
    @PostMapping(value = "/deleteBook")
    public ResponseEntity<Object> deleteBook(@RequestBody Book book)
    {
        try {
            boolean outcome = bd.deleteBook(book);

            if (!outcome)
            {
                return ResponseEntity.badRequest().body("The book to be deleted already does not exist");
            }
            return ResponseEntity.ok("Book correctly deleted");
        }
        catch (Exception e) {
            e.printStackTrace();
            return ResponseEntity.badRequest().body("Cannot delete the book due to the following exception: " + e.getCause());
        }
    }



}
