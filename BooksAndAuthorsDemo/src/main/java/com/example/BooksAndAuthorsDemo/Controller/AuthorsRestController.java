package com.example.BooksAndAuthorsDemo.Controller;


import com.example.BooksAndAuthorsDemo.model.DAOs.AuthorDao;
import com.example.BooksAndAuthorsDemo.model.DAOs.BookDao;
import com.example.BooksAndAuthorsDemo.model.Entities.Author;
import com.example.BooksAndAuthorsDemo.model.Entities.Book;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/authors")
public class AuthorsRestController
{
    @Autowired
    private AuthorDao ad;

    @Autowired
    private BookDao bd;

    @CrossOrigin(origins = "*")
    @GetMapping(value = "/getAll")
    public ResponseEntity<Object> findAllAuthors()
    {
        try {
            List<Author> outcome = ad.findAll();;

            if ( outcome == null)
            {
                return ResponseEntity.notFound().build();
            }
            return ResponseEntity.ok(outcome);
        }
        catch (Exception e) {
            e.printStackTrace();
            return ResponseEntity.badRequest().body("Cannot find any author due to the following exception: " + e.getCause());
        }
    }

    @CrossOrigin(origins = "*")
    @GetMapping(value = "/get/{id}")
    public ResponseEntity<Object> findSingleAuthor(@PathVariable int id)
    {
        try {
            Author outcome = ad.findById(id);;

            if ( outcome == null)
            {
                return ResponseEntity.notFound().build();
            }
            return ResponseEntity.ok(outcome);
        }
        catch (Exception e) {
            e.printStackTrace();
            return ResponseEntity.badRequest().body("Cannot find author due to the following exception: " + e.getCause());
        }
    }

    @CrossOrigin(origins = "*")
    @PostMapping(value = "/newAuthor")
    public ResponseEntity<Object> addNewAuthor(@RequestBody Author newAuthor)
    {
        try {
            Author outcome = ad.createOrUpdateAuthor(newAuthor);

            if ( outcome == null)
            {
                return ResponseEntity.badRequest().body("Author information is not correct or incomplete");
            }
            return ResponseEntity.ok(newAuthor);
        }
        catch (Exception e)
        {
            e.printStackTrace();
            return ResponseEntity.badRequest().body("Cannot create new author due to the following exception: " + e.getCause());
        }
    }

    @CrossOrigin(origins = "*")
    @PostMapping(value = "/updateAuthor")
    public ResponseEntity<Object> updateAuthor(@RequestBody Author updatedAuthor)
    {
        try {
            Author outcome = ad.createOrUpdateAuthor(updatedAuthor);

            if ( outcome == null)
            {
                return ResponseEntity.badRequest().body("Author information is not correct or incomplete");
            }
            return ResponseEntity.ok(outcome);
        }
        catch (Exception e) {
            e.printStackTrace();
            return ResponseEntity.badRequest().body("Cannot update the author due to the following exception: " + e.getCause());
        }
    }

    @CrossOrigin(origins = "*")
    @PostMapping(value = "/deleteAuthor")
    public ResponseEntity<Object> deleteAuthor(@RequestBody Author author)
    {
        try {
            boolean outcome = ad.deleteAuthor(author);

            if (!outcome)
            {
                return ResponseEntity.badRequest().body("The author to be deleted already does not exist");
            }
            return ResponseEntity.ok("Author correctly deleted");
        }
        catch (Exception e) {
            e.printStackTrace();
            return ResponseEntity.badRequest().body("Cannot delete the author due to the following exception: " + e.getCause());
        }
    }

    @CrossOrigin(origins = "*")
    @GetMapping(value = "/get/{id}/books")
    public ResponseEntity<Object> GetAllAuthorBooks(@PathVariable int id)
    {
        try {
            Author author = ad.findById(id);
            List<Book> outcome = bd.findAllByAuthor(author);

            if (outcome.size() == 0)
            {
                return ResponseEntity.badRequest().body("The author didn't write any book");
            }
            return ResponseEntity.ok(outcome);
        }
        catch (Exception e) {
            e.printStackTrace();
            return ResponseEntity.badRequest().body("Cannot fetch the author's books due to the following exception: " + e.getCause());
        }
    }

}

