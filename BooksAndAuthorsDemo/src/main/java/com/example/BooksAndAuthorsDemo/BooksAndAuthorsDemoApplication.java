package com.example.BooksAndAuthorsDemo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BooksAndAuthorsDemoApplication {

	public static void main(String[] args) {
		SpringApplication.run(BooksAndAuthorsDemoApplication.class, args);
	}

}
